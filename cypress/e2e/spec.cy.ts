describe("Admin login, create tribe, logout", () => {
  it("clicks the link 'Login'", () => {
    cy.visit("http://localhost:3000");

    cy.contains("Login").click();

    cy.get("#email").type("admi@danskebank.dk");
    cy.get("#password").type("Password1!");

    cy.contains("Sign In").click();

    cy.contains("Total Tribes").click();
    cy.contains("Add Tribe").click();

    cy.get("#tribeName").type("Cypress Tribe Test");
    cy.get("#tribeDescription").type(
      "Cypress end-to-end tribe test description"
    );
    cy.contains("Create Tribe").click();
    cy.contains("Success: Tribe created successfully");
    cy.contains("Go back to Dashboard").click();

    cy.contains("Total Tribes").click();
    cy.contains("Cypress Tribe Test");
    cy.contains("Logout").click();
  });
});
