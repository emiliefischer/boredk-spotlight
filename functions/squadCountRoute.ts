import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all squads
const fetchCountAllSquadsHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching squad count from database...");
    const query = `SELECT COUNT(*) AS squadCount FROM squads`;
    const squads = await executeQuery(query);
    console.log("Squads count fetched:", squads);

    return {
      statusCode: 200,
      body: JSON.stringify(squads),
    };
  } catch (error) {
    console.error("Error retrieving squads count:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllSquadsHandler);

export { handler };
