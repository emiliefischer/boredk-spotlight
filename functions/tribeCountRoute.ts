import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all tribes
const fetchCountAllTribesHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching tribe count from database...");
    const query = `SELECT COUNT(*) AS tribeCount FROM tribes`;
    const tribes = await executeQuery(query);
    console.log("Tribe count fetched:", tribes);

    return {
      statusCode: 200,
      body: JSON.stringify(tribes),
    };
  } catch (error) {
    console.error("Error retrieving tribe count:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllTribesHandler);

export { handler };
