import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all new products
const fetchCountAllProductsNewHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching new products from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsNewCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'new';
    `;
    const result = await executeQuery(query);
    const productsNewCount = result[0].productsNewCount;
    console.log("Count of new products fetched:", productsNewCount);
    return {
      statusCode: 200,
      body: JSON.stringify(productsNewCount),
    };
  } catch (error) {
    console.error("Error retrieving count of new products:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllProductsNewHandler);

export { handler };
