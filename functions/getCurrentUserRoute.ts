import express, { Request, Response } from "express";
const router = express.Router();
const { executeQuery } = require("../functions/mySQLConnect");
import { verifyToken } from "../middleware";

// Route til at hente brugeroplysninger for bruger som er logget ind
router.get("/", verifyToken, async (req: Request, res: Response) => {
  try {
    const userId = req.body.userId;

    // Hent brugeroplysninger fra databasen baseret på deres userId
    const users = await executeQuery("SELECT * FROM users WHERE user_id = ?", [
      userId,
    ]);

    if (users.length === 1) {
      const user = users[0];
      res.status(200).json({
        user_id: user.user_id,
        user_username: user.user_username,
        user_first_name: user.user_first_name,
        user_last_name: user.user_last_name,
        user_password: user.user_password,
        user_email: user.user_email,
        isSuperiorAdmin: user.user_isSuperiorAdmin,
        isSquadAdmin: user.user_isSquadAdmin,
        isTribeAdmin: user.user_isTribeAdmin,
      });
    } else {
      res.status(404).json({ error: "User not found" });
    }
  } catch (error) {
    console.error("Error fetching logged user data:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
