import { builder, Handler } from "@netlify/functions";
import { executeQuery } from "../functions/mySQLConnect";

// Get all users with their roles
const fetchUsersHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching users from database...");
    const query = `
      SELECT u.*, r.role_name
      FROM users u
      JOIN user_roles r ON u.user_role_fk = r.role_id
    `;
    const users = await executeQuery(query);
    console.log("Users fetched:", users);

    return {
      statusCode: 200,
      body: JSON.stringify(users),
    };
  } catch (error) {
    console.error("Error retrieving users:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchUsersHandler);

export { handler };
