import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchUserCountHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching user count from database...");
    const query = `SELECT COUNT(*) AS userCount FROM users`;
    const result = await executeQuery(query);
    const userCount = result[0].userCount;
    console.log("User count fetched:", userCount);

    return {
      statusCode: 200,
      body: JSON.stringify({ userCount }),
    };
  } catch (error) {
    console.error("Error retrieving user count:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchUserCountHandler);

export { handler };
