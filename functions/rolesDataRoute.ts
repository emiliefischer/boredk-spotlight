import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get all user roles
const fetchUserRolesHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching all user roles from database...");
    const userRoles = await executeQuery("SELECT * FROM user_roles");
    console.log("User roles fetched:", userRoles);

    return {
      statusCode: 200,
      body: JSON.stringify(userRoles),
    };
  } catch (error) {
    console.error("Error retrieving user roles:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchUserRolesHandler);

export { handler };
