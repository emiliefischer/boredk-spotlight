import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get all product statuses
const fetchProductStatusHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching product statuses from database...");
    const query = "SELECT * FROM product_status";
    const statuses = await executeQuery(query);

    console.log("All product statuses fetched:", statuses);
    return {
      statusCode: 200,
      body: JSON.stringify(statuses),
    };
  } catch (error) {
    console.error("Error retrieving product statuses:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchProductStatusHandler);

export { handler };
