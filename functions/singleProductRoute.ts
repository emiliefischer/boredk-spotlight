import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchSingleProductHandler: Handler = async (event, context) => {
  const productTitle = event.queryStringParameters?.productTitle;
  if (!productTitle) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: "Product title is required" }),
    };
  }

  try {
    console.log(`Fetching product with title: ${productTitle}`);
    const query = `
      SELECT 
        p.*, 
        ps.status_name,
        CONCAT(u.user_first_name, ' ', u.user_last_name) AS product_owner_name
      FROM 
        products p
      JOIN 
        product_status ps ON p.product_status_fk = ps.status_id
      LEFT JOIN 
        users u ON p.product_productowner_fk = u.user_id
      WHERE 
        LOWER(REPLACE(p.product_title, ' ', '-')) = ?
    `;
    const products = await executeQuery(query, [
      productTitle.toLowerCase().replace(/ /g, "-"),
    ]);
    if (products.length > 0) {
      console.log("Product fetched:", products[0]);
      return {
        statusCode: 200,
        body: JSON.stringify(products[0]),
      };
    } else {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: "Product not found" }),
      };
    }
  } catch (error) {
    console.error("Error retrieving product:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchSingleProductHandler);

export { handler };
