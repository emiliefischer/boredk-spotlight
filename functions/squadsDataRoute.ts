import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get all squads with the count of related products
const fetchSquadsHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching squads with product counts from database...");
    const query = `
      SELECT s.*, COUNT(p.product_id) AS total_products
      FROM squads s
      LEFT JOIN products p ON s.squad_id = p.product_squad_fk
      GROUP BY s.squad_id
    `;
    const squads = await executeQuery(query);
    console.log("Squads fetched:", squads);

    return {
      statusCode: 200,
      body: JSON.stringify(squads),
    };
  } catch (error) {
    console.error("Error retrieving squads:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchSquadsHandler);

export { handler };
