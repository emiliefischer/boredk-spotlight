import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all closed products
const fetchCountAllProductsClosedHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching closed products from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsNewCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'closed';
    `;
    const result = await executeQuery(query);
    const productsClosedCount = result[0].productsNewCount;
    console.log("Count of closed products fetched:", productsClosedCount);
    return {
      statusCode: 200,
      body: JSON.stringify(productsClosedCount),
    };
  } catch (error) {
    console.error("Error retrieving count of closed products:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllProductsClosedHandler);

export { handler };
