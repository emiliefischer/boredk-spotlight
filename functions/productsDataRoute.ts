import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchProductsHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching products from the database...");
    const query = `
      SELECT 
        p.*, 
        ps.status_name,
        CONCAT(u.user_first_name, ' ', u.user_last_name) AS product_owner_name
      FROM 
        products p
      JOIN 
        product_status ps ON p.product_status_fk = ps.status_id
      LEFT JOIN 
        users u ON p.product_productowner_fk = u.user_id
    `;
    const products = await executeQuery(query);
    console.log("Products fetched:", products);

    return {
      statusCode: 200,
      body: JSON.stringify(products),
    };
  } catch (error) {
    console.error("Error retrieving products:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchProductsHandler);

export { handler };
