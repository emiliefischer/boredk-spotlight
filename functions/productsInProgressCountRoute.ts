import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all products in progress
const fetchCountAllProductsInProgressHandler: Handler = async (
  event,
  context
) => {
  try {
    console.log("Fetching products in progress from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsInProgressCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'in progress';
    `;
    const result = await executeQuery(query);
    const productsInProgressCount = result[0].productsInProgressCount;
    console.log(
      "Count of products in progress fetched:",
      productsInProgressCount
    );
    return {
      statusCode: 200,
      body: JSON.stringify(productsInProgressCount),
    };
  } catch (error) {
    console.error("Error retrieving count of products in progress:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllProductsInProgressHandler);

export { handler };
