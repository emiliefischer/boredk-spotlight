import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get count of all products
const fetchCountAllProductsHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching product count from database...");
    const query = `SELECT COUNT(*) AS productCount FROM products`;
    const result = await executeQuery(query);
    const productCount = result[0].productCount;
    console.log("Products count fetched:", productCount);
    return {
      statusCode: 200,
      body: JSON.stringify(productCount),
    };
  } catch (error) {
    console.error("Error retrieving products count:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchCountAllProductsHandler);

export { handler };
