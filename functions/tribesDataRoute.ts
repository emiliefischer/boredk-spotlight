import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

// Get all tribes with tribe leader information and total squad count
const fetchTribesHandler: Handler = async (event, context) => {
  try {
    console.log("Fetching tribes from database...");
    const query = `
      SELECT
        t.*,
        GROUP_CONCAT(CONCAT(u.user_first_name, ' ', u.user_last_name) ORDER BY u.user_first_name ASC SEPARATOR ', ') AS tribe_leaders,
        COUNT(s.squad_id) AS total_squads
      FROM tribes t
      LEFT JOIN users u ON t.tribe_id = u.user_tribelead_fk
      LEFT JOIN squads s ON t.tribe_id = s.squad_tribe_fk
      GROUP BY t.tribe_id
    `;
    const products = await executeQuery(query);
    console.log("Tribes fetched:", products);

    return {
      statusCode: 200,
      body: JSON.stringify(products),
    };
  } catch (error) {
    console.error("Error retrieving tribes:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchTribesHandler);

export { handler };
