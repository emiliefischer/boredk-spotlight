import express from "express";
const { executeQuery } = require("../functions/mySQLConnect");

const router = express.Router();

// Delete a tribe
router.delete("/:tribeId", async (req, res) => {
  try {
    const { tribeId } = req.params;

    // Query to delete a tribe from the database
    const query = `
      DELETE FROM tribes WHERE tribe_id = ?
    `;

    await executeQuery(query, [tribeId]);

    res.status(200).json({ message: "Tribe deleted successfully" });
  } catch (error) {
    console.error("Error deleting tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
