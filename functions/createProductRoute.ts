import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
const { executeQuery } = require("../functions/mySQLConnect");

const router = express.Router();

// Create a new product
router.post("/", async (req: Request, res: Response) => {
  try {
    const {
      productTitle,
      productDescription,
      productImage,
      productGithubLink,
      productFigmaLink,
      productHostLink,
      productConfluenceLink,
      productSquadFk,
      productProductOwnerFk,
      productStatusFk,
    } = req.body;

    // Generate UUID without dashes
    const productId = uuidv4().replace(/-/g, "");

    // Check if required fields are provided
    if (
      !productTitle ||
      !productDescription ||
      !productSquadFk ||
      !productProductOwnerFk ||
      !productStatusFk
    ) {
      return res.status(400).json({ error: "Missing required fields" });
    }

    // Query to insert a new product into the database
    const query = `
      INSERT INTO products (
        product_id,
        product_created_at,
        product_updated_at,
        product_title,
        product_description,
        product_image,
        product_github_link,
        product_figma_link,
        product_host_link,
        product_confluence_link,
        product_squad_fk,
        product_productowner_fk,
        product_status_fk
      )
      VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
    `;

    await executeQuery(query, [
      productId,
      new Date(),
      new Date(),
      productTitle,
      productDescription,
      productImage || null,
      productGithubLink,
      productFigmaLink,
      productHostLink,
      productConfluenceLink,
      productSquadFk,
      productProductOwnerFk,
      productStatusFk,
    ]);

    res.status(201).json({ message: "Product created successfully" });
  } catch (error) {
    console.error("Error creating product:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
