import { builder, Handler } from "@netlify/functions";
const { executeQuery } = require("../functions/mySQLConnect");

const fetchTribeIdHandler: Handler = async (event, context) => {
  const tribeId = event.queryStringParameters?.tribeId;
  if (!tribeId) {
    return {
      statusCode: 400,
      body: JSON.stringify({ error: "Tribe id is required" }),
    };
  }
  try {
    console.log(`Fetching tribe with ID: ${tribeId}`);
    const query = `
      SELECT * FROM tribes WHERE tribe_id = ?
    `;
    const tribes = await executeQuery(query, [
      tribeId.toLowerCase().replace(/ /g, "-"),
    ]);
    if (tribes.length > 0) {
      console.log("Tribe fetched:", tribes[0]);
      return {
        statusCode: 200,
        body: JSON.stringify(tribes[0]),
      };
    } else {
      return {
        statusCode: 404,
        body: JSON.stringify({ error: "Tribe not found" }),
      };
    }
  } catch (error) {
    console.error("Error retrieving tribe:", error);
    return {
      statusCode: 500,
      body: JSON.stringify({ error: "Internal server error" }),
    };
  }
};

const handler = builder(fetchTribeIdHandler);

export { handler };
