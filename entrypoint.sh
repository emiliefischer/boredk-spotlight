#!/bin/bash
case "$RTE" in 
    dev )
        echo "** Development mode."
        npm run lint
        npm start &
        ts-node server.ts &
        wait
        ;;
    test )
        echo "** Test mode."
        npm run lint
        npm test
        ;;
    prod )
        echo "** Production mode."
        ;;
esac 
