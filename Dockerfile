FROM node:latest

WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .

COPY entrypoint.sh /

EXPOSE 3000

CMD ["/entrypoint.sh"]
