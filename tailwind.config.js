module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx,css}",
    "./node_modules/flowbite/**/*.js",
    "./node_modules/flowbite-react/lib/esm/**/*.js",
  ],
  darkMode: "class",
  theme: {
    colors: {
      dbWhite: "#FFFFFC",
      dbBackgroundColor: "#FAFAFA",
      dbBlue: "#002337",
      dbBrightBlue: "#007BC7",
      dbBrightBlueHover: "#006eb3",
      dbLightBlue: "#D7E9F1",
      dbLightBlueHover: "#c4dfea",
      dbSand: "#EFF2F5",
      dbGrey: "#CCD1D5",
      dbDarkGrey: "#5B5B5B",
      dbLabelGreen: "#268905",
      dbLabelPink: "#C6468D",
      dbLabelOrange: "#E27300",
      // Darkmode Colors
      dbBackgroundColorDark: "#111A20",
      dbCardBackgroundColorDark: "#162129",
      dbNavBackgroundColorDark: "#1B2933",
      dbBoxShadeOneDark: "#293F4E",
      // Searchfield
      dbSearchDark: "#334E61",
      // Text lys
      dbWhiteDark: "#E1E1E1",
      // Labels
      dbLabelGreenDark: "#8DBF7D",
      dbLabelPinkDark: "#DD9DC1",
      dbLabelOrangeDark: "#EBB47A",
      // Primary btn
      dbBrightBlueDark: "#80BDE3",
      // Secondary btn
      dbLightBlueDark: "#bad8e6",
    },
    extend: {
      fontSize: {
        xs: "0.625rem",
        sm: "0.75rem",
        md: "0.875rem",
        base: "1rem", //Paragraph
        h1: ["2.5rem", "2.7rem"],
        h2: "1.875rem",
        h3: "1.5rem",
        h4: "1.1rem",
        h5: "0.8rem",
        number: "4rem",
      },
      maxWidth: {
        readable: "65ch",
      },
    },
  },
  plugins: [require("flowbite/plugin")],
};
