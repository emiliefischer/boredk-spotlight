import express, { Request, Response, NextFunction } from "express";
import jwt from "jsonwebtoken";
const { executeQuery } = require("./mySQLConnect");
import rateLimit from "express-rate-limit";

const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100,
  standardHeaders: true,
  legacyHeaders: false,
});

export const verifyToken = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const token = req.cookies.token;

  if (!token) {
    return res.status(401).json({ error: "No token provided" });
  }

  jwt.verify(token, process.env.TOKEN_SECRET!, (err: any, decoded: any) => {
    if (err) {
      return res.status(401).json({ error: "Failed to authenticate token" });
    }

    req.body.userId = decoded.userId;
    req.body.isSuperiorAdmin = decoded.isSuperiorAdmin;
    req.body.isSquadAdmin = decoded.isSquadAdmin;
    req.body.isTribeAdmin = decoded.isTribeAdmin;

    const currentTime = Date.now() / 1000;
    if (decoded.exp < currentTime) {
      localStorage.removeItem("userRoles");

      return res
        .status(401)
        .json({ error: "Token has expired. Please log in again." });
    }

    next();
  });
};

export const verifyAdmin = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { userId, isSuperiorAdmin, isSquadAdmin, isTribeAdmin } = req.body;

  if (isSuperiorAdmin || isSquadAdmin || isTribeAdmin) {
    req.body.userRole = {
      isSuperiorAdmin,
      isSquadAdmin,
      isTribeAdmin,
    };
    next();
  } else {
    try {
      const users = await executeQuery(
        "SELECT user_isSuperiorAdmin, user_isSquadAdmin, user_isTribeAdmin FROM users WHERE id = ?",
        [userId]
      );
      if (users.length === 1) {
        const user = users[0];
        if (
          user.user_isSuperiorAdmin ||
          user.user_isSquadAdmin ||
          user.user_isTribeAdmin
        ) {
          req.body.userRole = {
            isSuperiorAdmin: user.user_isSuperiorAdmin,
            isSquadAdmin: user.user_isSquadAdmin,
            isTribeAdmin: user.user_isTribeAdmin,
          };
          next();
        } else {
          return res
            .status(403)
            .json({ error: "Access denied. Not an admin user." });
        }
      } else {
        return res.status(404).json({ error: "User not found" });
      }
    } catch (error) {
      console.error("Error verifying admin roles:", error);
      return res.status(500).json({ error: "Internal server error" });
    }
  }
};
