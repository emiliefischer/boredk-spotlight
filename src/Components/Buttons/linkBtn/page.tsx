import React from "react";
import { FaArrowRightLong } from "react-icons/fa6";

type LinkBtnProps = {
  content: string;
  linkTo: string;
};

const LinkButton = ({ content, linkTo }: LinkBtnProps) => {
  return (
    <div>
      <a
        className="flex items-center gap-2 text-sm text-dbBrightBlue dark:text-dbBrightBlueDark"
        href={linkTo}
      >
        {content}
        <FaArrowRightLong />
      </a>
    </div>
  );
};

export default LinkButton;
