import React from "react";
import { render, screen } from "@testing-library/react";
import LinkButton from "./page";

describe("LinkButton component", () => {
  test("renders with correct content and link", () => {
    const content = "Click me";
    const linkTo = "/some-link";
    render(<LinkButton content={content} linkTo={linkTo} />);

    const linkElement = screen.getByText(content);
    expect(linkElement).toBeInTheDocument();
    expect(linkElement.tagName).toBe("A");
    expect(linkElement).toHaveAttribute("href", linkTo);
  });
});
