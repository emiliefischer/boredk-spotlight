import React from "react";
import { FaArrowRightLong } from "react-icons/fa6";

type SecondaryBtnProps = {
  content: string;
  linkTo: string;
  onClick?: () => void;
};

const SecondaryButton = ({ content, linkTo, onClick }: SecondaryBtnProps) => (
  <a
    onClick={onClick}
    className="flex items-center gap-2 px-5 py-2 transition ease-in-out rounded-full shadow-md text-md w-fit text-dbBlue bg-dbLightBlue hover:bg-dbLightBlueHover dark:bg-dbLightBlueDark dark:hover:bg-dbWhiteDark"
    href={linkTo}
  >
    {content} <FaArrowRightLong />
  </a>
);

export default SecondaryButton;
