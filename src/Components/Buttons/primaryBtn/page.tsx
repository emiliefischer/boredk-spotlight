import React from "react";

type PrimaryBtnProps = {
  content: string;
  linkTo: string;
};

const PrimaryButton = ({ content, linkTo }: PrimaryBtnProps) => (
  <a
    className="flex gap-2 px-5 py-2 text-md transition ease-in-out rounded-full shadow-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
    href={linkTo}
  >
    {content}
  </a>
);

export default PrimaryButton;
