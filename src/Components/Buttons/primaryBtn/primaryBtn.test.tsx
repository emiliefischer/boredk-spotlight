import React from "react";
import { render, screen } from "@testing-library/react";
import PrimaryButton from "./page";

describe("PrimaryButton component", () => {
  test("renders button with correct content and link", () => {
    const content = "Click me";
    const linkTo = "/example";
    React.act(() => {
      render(<PrimaryButton content={content} linkTo={linkTo} />);
    });
    const buttonElement = screen.getByText(content);
    expect(buttonElement).toBeInTheDocument();
    expect(buttonElement.tagName).toBe("A");
    expect(buttonElement).toHaveAttribute("href", linkTo);
  });
});
