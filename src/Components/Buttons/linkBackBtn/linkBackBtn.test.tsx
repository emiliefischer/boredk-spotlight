import React from "react";
import { render, screen } from "@testing-library/react";
import LinkBackButton from "./page";

test("renders LinkBackButton with correct content and link", () => {
  const content = "Go back";
  const linkTo = "/home";

  render(<LinkBackButton content={content} linkTo={linkTo} />);

  const buttonElement = screen.getByText(content);
  expect(buttonElement).toBeInTheDocument();

  const linkElement = screen.getByRole("link", { name: content });
  expect(linkElement).toHaveAttribute("href", linkTo);
});
