import React from "react";
import { FaArrowLeftLong } from "react-icons/fa6";

type LinkBackBtnProps = {
  content: string;
  linkTo: string;
};

const LinkBackButton = ({ content, linkTo }: LinkBackBtnProps) => {
  return (
    <div>
      <a
        className="flex items-center gap-2 text-sm text-dbBrightBlue dark:text-dbBrightBlueDark"
        href={linkTo}
      >
        <FaArrowLeftLong />
        {content}
      </a>
    </div>
  );
};

export default LinkBackButton;
