import React from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { CiLogout } from "react-icons/ci";

const LogoutButton = () => {
  const navigate = useNavigate();

  const handleLogout = async () => {
    try {
      const response = await axios.post(
        "http://localhost:4000/api/logout",
        {},
        { withCredentials: true }
      );
      console.log("Logout successful!", response.data);

      // Clear localStorage
      localStorage.removeItem("token");
      localStorage.removeItem("userRoles");
      localStorage.removeItem("user");

      // Trigger a storage event to notify other tabs and components
      window.dispatchEvent(new Event("storage"));

      // Redirect to the front page
      navigate("/");
    } catch (err) {
      console.error("Error logging out:", err);
    }
  };

  return (
    <button
      onClick={handleLogout}
      className="flex items-center gap-2 px-5 py-2 transition ease-in-out rounded-full shadow-md text-md w-fit text-dbBlue bg-dbLightBlue hover:bg-dbLightBlueHover dark:bg-dbLightBlueDark dark:hover:bg-dbWhiteDark logout-button"
    >
      Logout <CiLogout />
    </button>
  );
};

export default LogoutButton;
