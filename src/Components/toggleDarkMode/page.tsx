import React from "react";
import { DarkThemeToggle } from "flowbite-react";

function ToggleDarkMode() {
  return (
    <div className="flex items-center justify-end px-4 py-2 bg-dbBlue dark:bg-dbBackgroundColorDark ">
      <DarkThemeToggle className="bg-dbWhite dark:bg-dbLightBlueDark text-dbBlue dark:text-dbBackgroundColorDark" />
    </div>
  );
}

export default ToggleDarkMode;
