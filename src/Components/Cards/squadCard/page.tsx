import React from "react";
import confluence from "../../../Assets/images/confluence.png";
import LinkButton from "../../Buttons/linkBtn/page";
import { Squad } from "../../../Types/squadInterface";
import { Link } from "react-router-dom";

interface SquadCardProps {
  squad: Squad;
}

const SquadCard: React.FC<SquadCardProps> = ({ squad }) => {
  return (
    <Link
      to={`/squads/${encodeURIComponent(
        squad.squad_name.toLowerCase().replace(/\s+/g, "-")
      )}`}
    >
      <div className="h-full py-6 rounded-sm bg-dbWhite dark:bg-dbNavBackgroundColorDark px-7">
        <p className="pb-5 text-md text-dbBlue dark:text-dbWhiteDark">
          {squad.squad_name}
        </p>
        <LinkButton linkTo="#" content="Read more" />
        <div className="flex gap-2 px-5 py-4 mt-5 rounded-full bg-dbSand dark:bg-dbBoxShadeOneDark">
          <img src={confluence} alt="" />
          <a
            className="self-center text-xs text-dbBrightBlue dark:text-dbLightBlueDark"
            href="#"
          >
            http://concfluencexyz
          </a>
        </div>
      </div>
    </Link>
  );
};

export default SquadCard;
