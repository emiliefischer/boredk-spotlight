import React from "react";
import { render, screen } from "@testing-library/react";
import SquadMemberCard from "./page";

describe("SquadMemberCard component", () => {
  it("renders email correctly", () => {
    render(<SquadMemberCard />);

    const emailLink = screen.getByRole("link", {
      name: /email@danskebank.dk/i,
    });
    expect(emailLink).toBeInTheDocument();
  });

  it("renders email icon correctly", () => {
    render(<SquadMemberCard />);

    const mailIcon = screen.getByTestId("mail-icon");
    expect(mailIcon).toBeInTheDocument();
    expect(mailIcon).toContainHTML("<svg");
  });
});
