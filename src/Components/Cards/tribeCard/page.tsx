import React from "react";
import confluence from "../../../Assets/images/confluence.png";
import LinkButton from "../../Buttons/linkBtn/page";
import { Tribe } from "../../../Types/tribeInterface";
import { Link } from "react-router-dom";
import { Button } from "flowbite-react";

interface TribeCardProps {
  tribe: Tribe;
}

const TribeCard: React.FC<TribeCardProps> = ({ tribe }) => {
  return (
    <Link
      to={`/tribes/${encodeURIComponent(
        tribe.tribe_name.toLowerCase().replace(/\s+/g, "-")
      )}`}
    >
      <div className="py-6 rounded-sm bg-dbWhite px-7 dark:bg-dbNavBackgroundColorDark">
        <p className="pb-5 text-md text-dbBlue dark:text-dbWhiteDark">
          {tribe.tribe_name}
        </p>
        <p className="pb-5 text-sm text-dbBlue dark:text-dbWhiteDark">
          <strong>TL:</strong> {tribe.tribe_leaders}
        </p>
        <LinkButton linkTo="#" content="Read more" />
        <div className="flex gap-2 px-5 py-4 mt-5 rounded-full bg-dbSand dark:bg-dbBoxShadeOneDark">
          <img src={confluence} alt="" />
          <button
            className="self-center text-xs text-dbBrightBlue dark:text-dbLightBlueDark"
            // href="#"
          >
            http://concfluencexyz
          </button>
        </div>
      </div>
    </Link>
  );
};

export default TribeCard;
