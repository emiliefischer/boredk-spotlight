import React from "react";
import { render, screen } from "@testing-library/react";
import LeaderCard from "./page";

describe("LeaderCard component", () => {
  const mockProps = {
    image: "mock_image_url",
    name: "John Doe",
    role: "Team Lead",
  };

  it("renders LeaderCard component with correct props", () => {
    render(<LeaderCard {...mockProps} />);

    expect(screen.getByText(mockProps.name)).toBeInTheDocument();
    expect(screen.getByText(mockProps.role)).toBeInTheDocument();

    const imageElement = screen.getByAltText("profile_picture");
    expect(imageElement).toBeInTheDocument();
    expect(imageElement.getAttribute("src")).toBe(mockProps.image);

    expect(imageElement).toHaveClass("w-48");
    expect(screen.getByText(mockProps.name)).toHaveClass("text-h3");
    expect(screen.getByText(mockProps.role)).toHaveClass("text-h4");
  });
});
