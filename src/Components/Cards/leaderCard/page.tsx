import React from "react";

type LeaderCardProps = {
  image: string;
  name: string;
  role: string;
};

const LeaderCard = ({ image, name, role }: LeaderCardProps) => {
  return (
    <div className="flex flex-col items-center text-dbWhite">
      <img src={image} alt="profile_picture" className="w-48 mx-auto my-4" />
      <p className="text-h3">{name}</p>
      <p className="text-h4">{role}</p>
    </div>
  );
};

export default LeaderCard;
