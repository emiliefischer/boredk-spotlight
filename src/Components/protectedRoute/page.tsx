import React, { ComponentType } from "react";
import { Navigate } from "react-router-dom";
import useAuth from "../../Hooks/useAuthHook";

interface ProtectedRouteProps {
  component: ComponentType<any>;
  [key: string]: any;
}

const ProtectedRoute: React.FC<ProtectedRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const loggedIn = useAuth();
  const userRoles = JSON.parse(localStorage.getItem("userRoles") || "{}");

  if (!loggedIn) {
    return <Navigate to="/" />;
  }

  if (
    !userRoles.isSuperiorAdmin &&
    !userRoles.isSquadAdmin &&
    !userRoles.isTribeAdmin
  ) {
    return <Navigate to="/" />;
  }

  return <Component {...rest} />;
};

export default ProtectedRoute;
