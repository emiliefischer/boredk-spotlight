import React, { ComponentType } from "react";
import { Navigate } from "react-router-dom";
import useAuth from "../../Hooks/useAuthHook";

interface UserProtectedRouteProps {
  component: ComponentType<any>;
  [key: string]: any;
}

const UserProtectedRoute: React.FC<UserProtectedRouteProps> = ({
  component: Component,
  ...rest
}) => {
  const loggedIn = useAuth();
  const userRoles = JSON.parse(localStorage.getItem("userRoles") || "{}");

  if (!loggedIn) {
    return <Navigate to="/" />;
  }

  if (
    userRoles.isSuperiorAdmin ||
    userRoles.isSquadAdmin ||
    userRoles.isTribeAdmin
  ) {
    return <Navigate to="/admin-dashboard" />;
  }

  return <Component {...rest} />;
};

export default UserProtectedRoute;
