import React, { useState } from "react";
import axios from "axios";
import useFetchSquads from "../../Hooks/useFetchSquads";
import useFetchRoles from "../../Hooks/useFetchRoles";
import { IoCreateOutline } from "react-icons/io5";
import { RxReset } from "react-icons/rx";
import PageInfo from "../../Components/pageInfo/page";
import LinkBackButton from "../../Components/Buttons/linkBackBtn/page";

interface FormData {
  user_username: string;
  user_first_name: string;
  user_last_name: string;
  user_email: string;
  user_password: string;
  confirmPassword?: string;
  user_isSuperiorAdmin: boolean;
  user_isSquadAdmin: boolean;
  user_isTribeAdmin: boolean;
  user_squad_fk: string;
  user_role_fk: string;
}

const CreateUserForm: React.FC = () => {
  const [formData, setFormData] = useState<FormData>({
    user_username: "",
    user_first_name: "",
    user_last_name: "",
    user_email: "",
    user_password: "",
    confirmPassword: "",
    user_isSuperiorAdmin: false,
    user_isSquadAdmin: false,
    user_isTribeAdmin: false,
    user_squad_fk: "",
    user_role_fk: "",
  });

  const [error, setError] = useState<string | null>(null);

  const { squads } = useFetchSquads();
  const { roles } = useFetchRoles();

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>
  ) => {
    const { name, value, type } = e.target;

    if (type === "checkbox") {
      const checked = (e.target as HTMLInputElement).checked;
      setFormData((prevData) => ({
        ...prevData,
        [name]: checked,
      }));

      // Uncheck other admin types if one is checked
      if (checked) {
        const otherAdminTypes = [
          "user_isSuperiorAdmin",
          "user_isSquadAdmin",
          "user_isTribeAdmin",
        ].filter((adminType) => adminType !== name);
        setFormData((prevData) => ({
          ...prevData,
          [otherAdminTypes[0]]: false,
          [otherAdminTypes[1]]: false,
        }));
      }

      // Update tribe lead based on user_isTribeAdmin checkbox
      if (name === "user_isTribeAdmin" && !checked) {
        // Reset tribe lead if user is not a tribe admin
        setFormData((prevData) => ({
          ...prevData,
          user_squad_fk: "",
        }));
      }
    } else {
      setFormData((prevData) => ({
        ...prevData,
        [name]: value,
      }));
    }
  };

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();

    try {
      // Get form data, excluding confirmPassword and user_tribelead_fk
      const { confirmPassword, ...dataToSubmit } = formData;

      // Submit user data without tribe lead FK or confirmPassword
      const response = await axios.post(
        "http://localhost:4000/api/createUser",
        dataToSubmit
      );

      // If successful, show alert and reset form
      alert("User created successfully");
      setError(null); // Reset error state on successful submission
    } catch (error: any) {
      // Handle and display error message
      if (error.response && error.response.data && error.response.data.error) {
        setError(error.response.data.error);
      } else {
        setError("Error creating user");
      }
      console.error("Error:", error);
    }
  };

  return (
    <div className="px-8 pt-4 pb-24 dark:bg-dbBackgroundColorDark dark:text-dbWhiteDark">
      <LinkBackButton linkTo="/admindashboard" content="Go back to Dashboard" />
      <PageInfo
        title="Create a new"
        secondTitle="User"
        content="Fill out the form and click on the submit button to create a new user."
      />
      <form onSubmit={handleSubmit} className="w-1/2 px-8">
        <div className="flex flex-col mt-4">
          <label htmlFor="user_username" className="italic text-md">
            Username
          </label>
          <input
            type="text"
            id="user_username"
            name="user_username"
            value={formData.user_username}
            onChange={handleChange}
            placeholder="Username"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex flex-col mt-2">
          <label htmlFor="user_first_name" className="italic text-md">
            First Name
          </label>
          <input
            type="text"
            id="user_first_name"
            name="user_first_name"
            value={formData.user_first_name}
            onChange={handleChange}
            placeholder="First Name"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex flex-col mt-2">
          <label htmlFor="user_last_name" className="italic text-md">
            Last Name
          </label>
          <input
            type="text"
            id="user_last_name"
            name="user_last_name"
            value={formData.user_last_name}
            onChange={handleChange}
            placeholder="Last Name"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex flex-col mt-2">
          <label htmlFor="user_email" className="italic text-md">
            Email
          </label>
          <input
            type="email"
            id="user_email"
            name="user_email"
            value={formData.user_email}
            onChange={handleChange}
            placeholder="Email"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex flex-col mt-2">
          <label htmlFor="user_password" className="italic text-md">
            Password
          </label>
          <input
            type="password"
            id="user_password"
            name="user_password"
            value={formData.user_password}
            onChange={handleChange}
            placeholder="Password"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex flex-col mt-2">
          <label htmlFor="confirmPassword" className="italic text-md">
            Confirm Password
          </label>
          <input
            type="password"
            id="confirmPassword"
            name="confirmPassword"
            value={formData.confirmPassword}
            onChange={handleChange}
            placeholder="Confirm Password"
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
            required
          />
        </div>
        <div className="flex items-center mt-4">
          <label className="mr-4 italic text-md">Admin Type:</label>
          <label className="mr-4">
            <input
              type="checkbox"
              name="user_isSuperiorAdmin"
              checked={formData.user_isSuperiorAdmin}
              onChange={handleChange}
              className="mr-2"
            />
            Superior Admin
          </label>
          <label className="mr-4">
            <input
              type="checkbox"
              name="user_isSquadAdmin"
              checked={formData.user_isSquadAdmin}
              onChange={handleChange}
              className="mr-2"
            />
            Squad Admin
          </label>
          <label>
            <input
              type="checkbox"
              name="user_isTribeAdmin"
              checked={formData.user_isTribeAdmin}
              onChange={handleChange}
              className="mr-2"
            />
            Tribe Admin
          </label>
        </div>
        <div className="flex flex-col mt-4">
          <label className="italic text-md">Squad FK:</label>
          <select
            name="user_squad_fk"
            value={formData.user_squad_fk}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          >
            <option value="">Select Squad</option>
            {squads.map((squad) => (
              <option key={squad.squad_id} value={squad.squad_id}>
                {squad.squad_name}
              </option>
            ))}
          </select>
        </div>
        <div className="flex flex-col mt-4">
          <label className="italic text-md">Role FK:</label>
          <select
            name="user_role_fk"
            value={formData.user_role_fk}
            onChange={handleChange}
            className="text-md dark:bg-dbNavBackgroundColorDark dark:text-dbWhiteDark"
          >
            <option value="">Select Role</option>
            {roles.map((role) => (
              <option key={role.role_id} value={role.role_id}>
                {role.role_name}
              </option>
            ))}
          </select>
        </div>
        {error && <div className="mt-4 text-red-500">{error}</div>}
        <div className="flex justify-end mt-8">
          <button
            type="button"
            className="flex items-center gap-2 px-5 py-2 mt-4 mr-4 transition ease-in-out rounded-full shadow-md text-md text-dbBlue bg-dbLightBlue dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
            onClick={() =>
              setFormData({
                user_username: "",
                user_first_name: "",
                user_last_name: "",
                user_email: "",
                user_password: "",
                confirmPassword: "",
                user_isSuperiorAdmin: false,
                user_isSquadAdmin: false,
                user_isTribeAdmin: false,
                user_squad_fk: "",
                user_role_fk: "",
              })
            }
          >
            <RxReset className="mr-2 " /> Reset
          </button>
          <button
            type="submit"
            className="flex items-center gap-2 px-5 py-2 mt-4 transition ease-in-out rounded-full shadow-md text-md text-dbSand bg-dbBrightBlue hover:bg-dbBrightBlueHover dark:text-dbCardBackgroundColorDark dark:bg-dbBrightBlueDark dark:hover:bg-dbLightBlue"
          >
            <IoCreateOutline className="" /> Create User
          </button>
        </div>
      </form>
    </div>
  );
};

export default CreateUserForm;
