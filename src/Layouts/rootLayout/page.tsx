import React from "react";
import ToggleDarkMode from "../../Components/toggleDarkMode/page";
import { Flowbite } from "flowbite-react";
import NavBarNotLogged from "../navBarNotLogged/page";
import NavBarLogged from "../navBarLogged/page";
import SideNavigationBar from "../sideBar/page";
import useAuth from "../../Hooks/useAuthHook";

type RootLayoutProps = {
  children: React.ReactNode;
};

const RootLayout = ({ children }: RootLayoutProps) => {
  const loggedIn = useAuth();

  return (
    <Flowbite>
      <div>
        <ToggleDarkMode />
        <div className="flex h-screen bg-dbBackgroundColor">
          <SideNavigationBar />
          <div className="flex flex-col flex-1 overflow-y-auto">
            {loggedIn ? <NavBarLogged /> : <NavBarNotLogged />}
            <div className="">{children}</div>
          </div>
        </div>
      </div>
    </Flowbite>
  );
};

export default RootLayout;
