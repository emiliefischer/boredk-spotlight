import React, { useEffect, useState } from "react";
import dbLogo from "../../Assets/images/logo.png";
import useAuth from "../../Hooks/useAuthHook";
import LogoutButton from "../../Components/Buttons/logoutBtn/page";
import { CiBank } from "react-icons/ci";
import { GoPeople } from "react-icons/go";
import { PiProjectorScreenLight } from "react-icons/pi";
import useFetchCookieUser from "../../Hooks/useFetchCookieUser";

const SideBar = () => {
  const loggedIn = useAuth();
  const [userRoles, setUserRoles] = useState({
    isSuperiorAdmin: false,
    isSquadAdmin: false,
    isTribeAdmin: false,
  });
  const { user } = useFetchCookieUser();

  useEffect(() => {
    // Example logic to set user roles based on user data fetched
    if (user) {
      setUserRoles({
        isSuperiorAdmin: user.isSuperiorAdmin, // Assuming isAdmin is a property indicating admin status
        isSquadAdmin: user.isSquadAdmin,
        isTribeAdmin: user.isTribeAdmin,
      });
    }
  }, [user]);

  return (
    <div className="flex flex-col w-64 border-r border-dbGrey dark:border-none md:flex bg-dbWhite dark:bg-dbNavBackgroundColorDark">
      <a href="/" className="flex items-center justify-center h-16 p-4 pl-4">
        <img
          src={dbLogo}
          alt="Logo"
          className="border border-1 border-dbBlue"
        />
      </a>
      <div className="flex flex-col justify-between flex-1 overflow-y-auto">
        <nav className="px-2 py-4">
          {loggedIn ? (
            <div className="px-2 py-4">
              <a
                href="/tribes"
                className="flex items-center px-4 py-2 rounded-sm hover:ease-in-out hover:transition text-dbBlue hover:bg-dbSand dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
              >
                <CiBank className="mr-2" />
                Tribes
              </a>
              <a
                href="/squads"
                className="flex items-center px-4 py-2 mt-2 rounded-sm text-dbBlue hover:bg-dbSand hover:ease-in-out hover:transition dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
              >
                <GoPeople className="mr-2" />
                Squads
              </a>
              <a
                href="/products"
                className="flex items-center px-4 py-2 mt-2 rounded-sm text-dbBlue hover:bg-dbSand hover:ease-in-out hover:transition dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
              >
                <PiProjectorScreenLight className="mr-2" />
                Products
              </a>
            </div>
          ) : null}
          {/* {loggedIn && (
            <a
              href="/admindashboard"
              className="flex items-center px-4 py-2 mt-2 rounded-sm text-dbBlue hover:bg-dbSand hover:ease-in-out hover:transition dark:text-dbWhiteDark dark:hover:bg-dbBackgroundColorDark"
            >
              Admin Dashboard
            </a>
          )} */}
        </nav>
        {loggedIn ? (
          <div className="p-4 mb-10 ml-4">{<LogoutButton />}</div>
        ) : null}
      </div>
    </div>
  );
};

export default SideBar;
