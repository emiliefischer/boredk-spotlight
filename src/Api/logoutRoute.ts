import express, { Request, Response } from "express";
const router = express.Router();

router.post("/", (req: Request, res: Response) => {
  // Ensure req.cookies is defined
  if (!req.cookies) {
    return res.status(400).json({ error: "Cookies are not enabled" });
  }

  const token = req.cookies.token;

  if (!token) {
    return res
      .status(400)
      .json({ error: "No token found, user is not logged in" });
  }

  // Clear the token cookie
  res.clearCookie("token", {
    httpOnly: true,
    secure: process.env.NODE_ENV === "production",
    sameSite: "strict",
  });

  res.status(200).json({ message: "Logout successful", redirect: "/" });
});

module.exports = router;
