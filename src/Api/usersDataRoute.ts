const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get all users with their roles
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching users from database...");
    const query = `
      SELECT u.*, r.role_name
      FROM users u
      JOIN user_roles r ON u.user_role_fk = r.role_id
    `;
    const users = await executeQuery(query);
    console.log("Users fetched:", users);
    res.json(users);
  } catch (error) {
    console.error("Error retrieving users:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
