import express, { Request, Response } from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Update a tribe
router.put("/:tribeId", async (req: Request, res: Response) => {
  try {
    const { tribeId } = req.params;
    const { tribeName, tribeDescription } = req.body;

    // Log the incoming request for debugging
    console.log(
      "Received update request for tribeId:",
      tribeId,
      "with data:",
      req.body
    );

    // Construct the SET clause of the SQL query dynamically based on provided fields
    let setClause = "";
    const params: (string | number)[] = [];

    if (tribeName) {
      setClause += "tribe_name = ?, ";
      params.push(tribeName);
    }

    if (tribeDescription) {
      setClause += "tribe_description = ?, ";
      params.push(tribeDescription);
    }

    // Remove trailing comma and space from setClause
    setClause = setClause.replace(/, $/, "");

    // Check if at least one field is provided
    if (!setClause) {
      return res
        .status(400)
        .json({ error: "At least one field is required for update" });
    }

    // Query to update a tribe in the database
    const query = `
      UPDATE tribes
      SET ${setClause}
      WHERE tribe_id = ?
    `;

    // Add tribeId to params array
    params.push(tribeId);

    // Execute the query
    const result = await executeQuery(query, params);

    // Log the result for debugging
    console.log("Update query result:", result);

    res.status(200).json({ message: "Tribe updated successfully" });
  } catch (error) {
    console.error("Error updating tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
