const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get all products with status_name and product_owner information
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching products from database...");
    const query = `
      SELECT 
        p.*, 
        ps.status_name,
        CONCAT(u.user_first_name, ' ', u.user_last_name) AS product_owner_name
      FROM 
        products p
      JOIN 
        product_status ps ON p.product_status_fk = ps.status_id
      LEFT JOIN 
        users u ON p.product_productowner_fk = u.user_id
    `;
    const products = await executeQuery(query);
    console.log("products fetched:", products);
    res.json(products);
  } catch (error) {
    console.error("Error retrieving products:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
