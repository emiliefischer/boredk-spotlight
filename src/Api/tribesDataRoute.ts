const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get all tribes with tribe leader information and total squad count
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching tribes from database...");
    const query = `
      SELECT
        t.*,
        GROUP_CONCAT(CONCAT(u.user_first_name, ' ', u.user_last_name) ORDER BY u.user_first_name ASC SEPARATOR ', ') AS tribe_leaders,
        COUNT(s.squad_id) AS total_squads
      FROM tribes t
      LEFT JOIN users u ON t.tribe_id = u.user_tribelead_fk
      LEFT JOIN squads s ON t.tribe_id = s.squad_tribe_fk
      GROUP BY t.tribe_id
    `;
    const tribes = await executeQuery(query);
    console.log("Tribes fetched:", tribes);
    res.json(tribes);
  } catch (error) {
    console.error("Error retrieving tribes:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
