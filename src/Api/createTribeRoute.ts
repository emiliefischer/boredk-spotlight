import express, { Request, Response } from "express";
import { v4 as uuidv4 } from "uuid";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Create a new tribe
router.post("/", async (req: Request, res: Response) => {
  try {
    const { tribeName, tribeDescription } = req.body;

    // Generate UUID without dashes
    const tribeId = uuidv4().replace(/-/g, "");

    console.log("Generated UUID:", tribeId);

    // Check if required fields are provided
    if (!tribeName || !tribeDescription) {
      return res.status(400).json({ error: "Missing required fields" });
    }

    // Query to insert a new tribe into the database
    const query = `
      INSERT INTO tribes (tribe_id, tribe_created_at, tribe_updated_at, tribe_name, tribe_description)
      VALUES (?, ?, ?, ?, ?)
    `;

    await executeQuery(query, [
      tribeId,
      new Date(),
      new Date(),
      tribeName,
      tribeDescription,
    ]);

    res.status(201).json({ message: "Tribe created successfully" });
  } catch (error) {
    console.error("Error creating tribe:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
