const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get all squads with the count of related products
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching squads with product counts from database...");
    const query = `
      SELECT s.*, COUNT(p.product_id) AS total_products
      FROM squads s
      LEFT JOIN products p ON s.squad_id = p.product_squad_fk
      GROUP BY s.squad_id
    `;
    const squads = await executeQuery(query);
    console.log("Squads with product counts fetched:", squads);
    res.json(squads);
  } catch (error) {
    console.error("Error retrieving squads:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
