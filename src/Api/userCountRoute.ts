const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all users
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching user count from database...");
    const query = `SELECT COUNT(*) AS userCount FROM users`;
    const result = await executeQuery(query);
    const userCount = result[0].userCount;
    console.log("User count fetched:", userCount);
    res.json({ userCount });
  } catch (error) {
    console.error("Error retrieving user count:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
