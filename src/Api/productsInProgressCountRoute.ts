const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all products in progress
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching products in progress from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsInProgressCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'in progress';
    `;
    const result = await executeQuery(query);
    const productsInProgressCount = result[0].productsInProgressCount;
    console.log(
      "Count of products in progress fetched:",
      productsInProgressCount
    );
    res.json({ productsInProgressCount });
  } catch (error) {
    console.error("Error retrieving count of products in progress:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
