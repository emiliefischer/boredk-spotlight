import express from "express";
import { executeQuery } from "../../mySQLConnect";

const router = express.Router();

// Get all product statuses
router.get("/", async (req, res) => {
  try {
    // Query to fetch all product statuses from the database
    const query = "SELECT * FROM product_status";

    const statuses = await executeQuery(query);

    res.status(200).json(statuses);
  } catch (error) {
    console.error("Error fetching product statuses:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
