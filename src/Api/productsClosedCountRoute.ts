const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all closed products
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching closed products from database...");
    const query = `
      SELECT COUNT(products.product_id) AS productsNewCount
      FROM products
      INNER JOIN product_status ON products.product_status_fk = product_status.status_id
      WHERE product_status.status_name = 'closed';
    `;
    const result = await executeQuery(query);
    const productsClosedCount = result[0].productsNewCount;
    console.log("Count of closed products fetched:", productsClosedCount);
    res.json({ productsClosedCount });
  } catch (error) {
    console.error("Error retrieving count of closed products:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
