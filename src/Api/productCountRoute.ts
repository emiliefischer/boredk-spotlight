const express = require("express");
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");
import { Response, Request } from "express";

// Get count of all products
router.get("/", async (req: Request, res: Response) => {
  try {
    console.log("Fetching product count from database...");
    const query = `SELECT COUNT(*) AS productCount FROM products`;
    const result = await executeQuery(query);
    const productCount = result[0].productCount;
    console.log("Product count fetched:", productCount);
    res.json({ productCount });
  } catch (error) {
    console.error("Error retrieving product count:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
