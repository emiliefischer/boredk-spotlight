import express, { Request, Response } from "express";
const router = express.Router();
const { executeQuery } = require("../../mySQLConnect");

// Fetch user roles
router.get("/", async (req: Request, res: Response) => {
  try {
    // Fetch user roles from the database
    const userRoles = await executeQuery("SELECT * FROM user_roles");
    res.json(userRoles);
  } catch (error) {
    console.error("Error fetching user roles:", error);
    res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
