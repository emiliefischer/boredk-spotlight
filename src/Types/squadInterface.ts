export interface Squad {
  squad_id: string;
  squad_name: string;
  squad_description: string;
  squad_created_at: string;
  total_products: string;
  squad_tribe_fk: string;
}
