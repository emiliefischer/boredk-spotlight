export interface User {
  user_id: string;
  user_username: string;
  user_first_name: string;
  user_last_name: string;
  user_email: string;
  created_at: string;
  updated_at: string;
  user_squad_fk: string;
  user_role_fk: string;
  user_password: string;
  isSuperiorAdmin: boolean;
  isSquadAdmin: boolean;
  isTribeAdmin: boolean;
}
