import { useState, useEffect } from "react";
import axios from "axios";
import { Tribe } from "../Types/tribeInterface";

const useFetchTribes = (limit?: number) => {
  const [tribes, setTribes] = useState<Tribe[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchTribes = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/tribesData`;
        } else {
          apiUrl = `${baseUrl}/tribesDataRoute`;
        }

        const response = await axios.get<Tribe[]>(apiUrl);
        let sortedTribes = response.data.sort(
          (a, b) =>
            new Date(b.created_at).getTime() - new Date(a.created_at).getTime()
        );
        if (limit) {
          sortedTribes = sortedTribes.slice(0, limit);
        }
        setTribes(sortedTribes);
      } catch (error) {
        console.error("Error fetching tribes:", error);
        setError("Error fetching tribes");
      } finally {
        setLoading(false);
      }
    };

    fetchTribes();
  }, [limit]);

  return { tribes, loading, error };
};

export default useFetchTribes;
