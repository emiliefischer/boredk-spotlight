import { useState, useEffect } from "react";
import axios from "axios";
import { User } from "../Types/userInterface";

const useFetchUsers = (limit?: number) => {
  const [users, setUsers] = useState<User[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/usersData`;
        } else {
          apiUrl = `${baseUrl}/usersDataRoute`;
        }

        const response = await axios.get<User[]>(apiUrl);
        let sortedUsers = response.data.sort(
          (a, b) =>
            new Date(b.created_at).getTime() - new Date(a.updated_at).getTime()
        );
        if (limit) {
          sortedUsers = sortedUsers.slice(0, limit);
        }
        setUsers(sortedUsers);
      } catch (error) {
        console.error("Error fetching users:", error);
        setError("Error fetching users");
      } finally {
        setLoading(false);
      }
    };

    fetchUsers();
  }, [limit]);

  return { users, loading, error };
};

export default useFetchUsers;
