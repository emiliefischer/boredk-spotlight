import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsNew = () => {
  const [productsNewCount, setProductsNewCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductNewCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/products/productsNewCount`;
        } else {
          apiUrl = `${baseUrl}/productsNewCountRoute`;
        }

        const response = await axios.get<{ productsNewCount: number }>(apiUrl);
        setProductsNewCount(response.data.productsNewCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching products in progress count:", error);
        setLoading(false);
      }
    };

    fetchProductNewCount();
  }, []);

  return { productsNewCount, loading };
};

export default useCountProductsNew;
