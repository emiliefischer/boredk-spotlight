import { useState, useEffect } from "react";

const useAuth = () => {
  const isUserLoggedIn = (): boolean => {
    const token = localStorage.getItem("token");
    return !!token;
  };

  const [loggedIn, setLoggedIn] = useState<boolean>(isUserLoggedIn());

  useEffect(() => {
    const handleStorageChange = () => {
      setLoggedIn(isUserLoggedIn());
    };

    window.addEventListener("storage", handleStorageChange);

    return () => {
      window.removeEventListener("storage", handleStorageChange);
    };
  }, []);

  return loggedIn;
};

export default useAuth;
