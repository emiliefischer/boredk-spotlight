import { useState, useEffect } from "react";
import { User } from "../Types/userInterface";

const useFetchCookieUser = () => {
  const [user, setUser] = useState<User | null>(null);

  useEffect(() => {
    const user = localStorage.getItem("user");
    if (user) {
      setUser(JSON.parse(user));
    }
  }, []);

  return { user };
};

export default useFetchCookieUser;
