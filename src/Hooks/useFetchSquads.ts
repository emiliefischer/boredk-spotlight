import { useState, useEffect } from "react";
import axios from "axios";
import { Squad } from "../Types/squadInterface";

const useFetchSquads = (limit?: number) => {
  const [squads, setSquads] = useState<Squad[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchSquads = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/squadsData`;
        } else {
          apiUrl = `${baseUrl}/squadsDataRoute`;
        }

        const response = await axios.get<Squad[]>(apiUrl);
        let sortedSquads = response.data.sort(
          (a, b) =>
            new Date(b.squad_created_at).getTime() -
            new Date(a.squad_created_at).getTime()
        );
        if (limit) {
          sortedSquads = sortedSquads.slice(0, limit);
        }
        setSquads(sortedSquads);
      } catch (error) {
        console.error("Error fetching squads:", error);
        setError("Error fetching squads");
      } finally {
        setLoading(false);
      }
    };

    fetchSquads();
  }, [limit]);

  return { squads, loading, error };
};

export default useFetchSquads;
