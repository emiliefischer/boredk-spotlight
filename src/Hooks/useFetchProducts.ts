import { useState, useEffect } from "react";
import axios from "axios";
import { Product } from "../Types/productInterface";

const useFetchProducts = (limit?: number) => {
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchProducts = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/productsData`;
        } else {
          apiUrl = `${baseUrl}/productsDataRoute`;
        }

        const response = await axios.get<Product[]>(apiUrl);
        let sortedProducts = response.data.sort(
          (a, b) =>
            new Date(b.product_created_at).getTime() -
            new Date(a.product_created_at).getTime()
        );
        if (limit) {
          sortedProducts = sortedProducts.slice(0, limit);
        }
        setProducts(sortedProducts);
      } catch (error) {
        console.error("Error fetching products:", error);
        setError("Error fetching products");
      } finally {
        setLoading(false);
      }
    };

    fetchProducts();
  }, [limit]);

  return { products, loading, error };
};

export default useFetchProducts;
