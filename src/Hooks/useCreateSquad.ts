import axios, { AxiosError } from "axios";
import { useState } from "react";

interface CreateSquadResponse {
  message: string;
}

const useCreateSquad = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const createSquad = async (formData: {
    squadName: string;
    squadDescription: string;
    tribeId: string;
    leaderUserId: string;
  }) => {
    setLoading(true);
    setError(null);
    setSuccessMessage(null);

    try {
      const response = await axios.post<CreateSquadResponse>(
        "http://localhost:4000/api/createSquad",
        {
          squadName: formData.squadName,
          squadDescription: formData.squadDescription,
          tribeId: formData.tribeId,
          leaderUserId: formData.leaderUserId,
        },
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      setSuccessMessage(response.data.message);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.data) {
          const responseData = axiosError.response.data as { error: string };
          setError(responseData.error);
        } else {
          setError("An error occurred while processing your request");
        }
      } else {
        setError("An error occurred while processing your request");
      }
    } finally {
      setLoading(false);
    }
  };

  const resetSuccessMessage = () => {
    setSuccessMessage(null);
  };

  return { createSquad, loading, error, successMessage, resetSuccessMessage };
};

export default useCreateSquad;
