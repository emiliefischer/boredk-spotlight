import axios from "axios";
import { useEffect, useState } from "react";

const useCountTribes = () => {
  const [tribeCount, setTribeCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/tribes/tribeCount`;
        } else {
          apiUrl = `${baseUrl}/tribeCountRoute`;
        }

        const response = await axios.get<{ tribeCount: number }>(apiUrl);
        setTribeCount(response.data.tribeCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching product count:", error);
        setLoading(false);
      }
    };

    fetchProductCount();
  }, []);
  return { tribeCount, loading };
};

export default useCountTribes;
