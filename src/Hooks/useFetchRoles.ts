import { useState, useEffect } from "react";
import axios from "axios";

const useFetchRoles = () => {
  const [roles, setRoles] = useState<any[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchRoles = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/rolesData`;
        } else {
          apiUrl = `${baseUrl}/rolesDataRoute`;
        }
        const response = await axios.get<any[]>(apiUrl);

        setRoles(response.data);
      } catch (error) {
        console.error("Error fetching roles:", error);
        setError("Error fetching roles");
      } finally {
        setLoading(false);
      }
    };

    fetchRoles();
  }, []);

  return { roles, loading, error };
};

export default useFetchRoles;
