import { useState, useEffect } from "react";
import axios from "axios";
import { Squad } from "../Types/squadInterface";

const useFetchSingleSquad = (squadId?: number) => {
  const [squad, setSquad] = useState<Squad | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchSquad = async () => {
      if (!squadId) {
        setLoading(false);
        return;
      }

      try {
        const response = await axios.get<Squad>(
          `http://localhost:4000/api/singleSquadByIdRoute/${squadId}`
        );
        setSquad(response.data);
      } catch (error) {
        console.error("Error fetching squad:", error);
        setError("Error fetching squad");
      } finally {
        setLoading(false);
      }
    };

    fetchSquad();
  }, [squadId]);

  return { squad, loading, error };
};

export default useFetchSingleSquad;
