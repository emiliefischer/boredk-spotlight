import axios, { AxiosError } from "axios";
import { useState } from "react";

interface CreateTribeResponse {
  message: string;
}

const useCreateTribe = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const [successMessage, setSuccessMessage] = useState<string | null>(null);

  const createTribe = async (formData: {
    name: string;
    description: string;
  }) => {
    setLoading(true);
    setError(null);
    setSuccessMessage(null);

    try {
      const response = await axios.post<CreateTribeResponse>(
        "http://localhost:4000/api/createTribe",
        {
          tribeName: formData.name,
          tribeDescription: formData.description,
        },
        {
          headers: { "Content-Type": "application/json" },
        }
      );
      setSuccessMessage(response.data.message);
    } catch (error) {
      if (axios.isAxiosError(error)) {
        const axiosError = error as AxiosError;
        if (axiosError.response && axiosError.response.data) {
          const responseData = axiosError.response.data as { error: string };
          setError(responseData.error);
        } else {
          setError("An error occurred while processing your request");
        }
      } else {
        setError("An error occurred while processing your request");
      }
    } finally {
      setLoading(false);
    }
  };

  const resetSuccessMessage = () => {
    setSuccessMessage(null);
  };

  return { createTribe, loading, error, successMessage, resetSuccessMessage };
};

export default useCreateTribe;
