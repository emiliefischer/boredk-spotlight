import axios from "axios";
import { useState } from "react";

const useDeleteSquad = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const deleteSquad = async (squadId: string) => {
    setLoading(true);
    setError(null);

    try {
      await axios.delete(`http://localhost:4000/api/deleteSquad/${squadId}`);
    } catch (err) {
      if (axios.isAxiosError(err)) {
        setError(
          err.response?.data?.error ||
            "An error occurred while deleting the squad"
        );
      } else if (err instanceof Error) {
        setError(err.message);
      } else {
        setError("An unknown error occurred");
      }
    } finally {
      setLoading(false);
    }
  };

  return { deleteSquad, loading, error };
};

export default useDeleteSquad;
