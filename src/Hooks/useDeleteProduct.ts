import axios from "axios";
import { useState } from "react";

const useDeleteProduct = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const deleteProduct = async (productId: string) => {
    setLoading(true);
    setError(null);

    try {
      await axios.delete(
        `http://localhost:4000/api/deleteProduct/${productId}`
      );
    } catch (err) {
      if (axios.isAxiosError(err)) {
        setError(
          err.response?.data?.error ||
            "An error occurred while deleting the product"
        );
      } else if (err instanceof Error) {
        setError(err.message);
      } else {
        setError("An unknown error occurred");
      }
    } finally {
      setLoading(false);
    }
  };

  return { deleteProduct, loading, error };
};

export default useDeleteProduct;
