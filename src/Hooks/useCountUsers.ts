import axios from "axios";
import { useEffect, useState } from "react";

const useCountUsers = () => {
  const [userCount, setUserCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchUserCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/users/userCount`;
        } else {
          apiUrl = `${baseUrl}/userCountRoute`;
        }

        const response = await axios.get<{ userCount: number }>(apiUrl);
        setUserCount(response.data.userCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching user count:", error);
        setLoading(false);
      }
    };

    fetchUserCount();
  }, []);
  return { userCount, loading };
};

export default useCountUsers;
