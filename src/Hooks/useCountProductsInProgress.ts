import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsInProgress = () => {
  const [productsInProgressCount, setProductsInProgressCount] = useState<
    number | null
  >(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductsInProgressCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/products/productsInProgressCount`;
        } else {
          apiUrl = `${baseUrl}/productsInProgressCountRoute`;
        }

        const response = await axios.get<{ productsInProgressCount: number }>(
          apiUrl
        );
        setProductsInProgressCount(response.data.productsInProgressCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching products in progress count:", error);
        setLoading(false);
      }
    };

    fetchProductsInProgressCount();
  }, []);

  return { productsInProgressCount, loading };
};

export default useCountProductsInProgress;
