import { useEffect, useState } from "react";
import axios from "axios";

interface RoleNameResponse {
  roleName: string;
}

const useFetchRoleName = () => {
  const [roleName, setRoleName] = useState<string>("");

  useEffect(() => {
    const fetchRoleName = async () => {
      try {
        const user = JSON.parse(localStorage.getItem("user") || "{}");
        console.log("User from localStorage:", user);

        const roleId = user?.user_role_fk;
        console.log("Role ID from user:", roleId);

        if (roleId) {
          const response = await axios.get<RoleNameResponse>(
            `http://localhost:4000/api/roleById/${roleId}`
          );
          console.log("API response:", response.data);

          setRoleName(response.data.roleName);
        } else {
          console.error("Role ID is invalid or not found");
        }
      } catch (error) {
        console.error("Error fetching role name:", error);
      }
    };

    fetchRoleName();
  }, []);

  return { roleName };
};

export default useFetchRoleName;
