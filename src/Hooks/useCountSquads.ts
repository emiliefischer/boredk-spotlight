import { useState, useEffect } from "react";
import axios from "axios";

const useCountSquads = () => {
  const [squadCount, setSquadCount] = useState<number | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const fetchSquadCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/squads/squadCount`;
        } else {
          apiUrl = `${baseUrl}/squadCountRoute`;
        }

        const response = await axios.get<{ squadCount: number }>(apiUrl);
        setSquadCount(response.data.squadCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching squad count:", error);
        setError("Error fetching squad count");
        setLoading(false);
      }
    };

    fetchSquadCount();
  }, []);

  return { squadCount, loading, error };
};

export default useCountSquads;
