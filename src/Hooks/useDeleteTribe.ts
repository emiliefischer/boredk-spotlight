import axios from "axios";
import { useState } from "react";

const useDeleteTribe = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  const deleteTribe = async (tribeId: string) => {
    setLoading(true);
    setError(null);

    try {
      await axios.delete(`http://localhost:4000/api/deleteTribe/${tribeId}`);
    } catch (err) {
      if (axios.isAxiosError(err)) {
        setError(
          err.response?.data?.error ||
            "An error occurred while deleting the tribe"
        );
      } else if (err instanceof Error) {
        setError(err.message);
      } else {
        setError("An unknown error occurred");
      }
    } finally {
      setLoading(false);
    }
  };

  return { deleteTribe, loading, error };
};

export default useDeleteTribe;
