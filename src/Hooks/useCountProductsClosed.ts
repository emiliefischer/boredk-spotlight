import axios from "axios";
import { useEffect, useState } from "react";

const useCountProductsClosed = () => {
  const [productsClosedCount, setProductsClosedCount] = useState<number | null>(
    null
  );
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    const fetchProductClosedCount = async () => {
      try {
        const baseUrl =
          process.env.NODE_ENV === "development"
            ? process.env.REACT_APP_API_BASE_URL_LOCAL
            : process.env.REACT_APP_API_BASE_URL_PROD;

        let apiUrl;
        if (process.env.NODE_ENV === "development") {
          apiUrl = `${baseUrl}/products/productsClosedCount`;
        } else {
          apiUrl = `${baseUrl}/productsClosedCountRoute`;
        }

        const response = await axios.get<{ productsClosedCount: number }>(
          apiUrl
        );
        setProductsClosedCount(response.data.productsClosedCount);
        setLoading(false);
      } catch (error) {
        console.error("Error fetching closed products count:", error);
        setLoading(false);
      }
    };

    fetchProductClosedCount();
  }, []);

  return { productsClosedCount, loading };
};

export default useCountProductsClosed;
