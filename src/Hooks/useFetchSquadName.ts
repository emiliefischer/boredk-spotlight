import { useEffect, useState } from "react";
import axios from "axios";

interface SquadNameResponse {
  squadName: string;
}

const useFetchSquadName = () => {
  const [squadName, setSquadName] = useState<string>("");

  useEffect(() => {
    const fetchSquadName = async () => {
      try {
        const user = JSON.parse(localStorage.getItem("user") || "{}");
        console.log("User from localStorage:", user);

        const squadId = user?.user_squad_fk;
        console.log("Squad ID from user:", squadId);

        if (squadId) {
          const response = await axios.get<SquadNameResponse>(
            `http://localhost:4000/api/squadById/${squadId}`
          );
          console.log("API response:", response.data);

          setSquadName(response.data.squadName);
        } else {
          console.error("Squad ID is invalid or not found");
        }
      } catch (error) {
        console.error("Error fetching squad name:", error);
      }
    };

    fetchSquadName();
  }, []);

  return { squadName };
};

export default useFetchSquadName;
