-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema BOREDK_Spotlight_Platform
-- -----------------------------------------------------

CREATE SCHEMA IF NOT EXISTS `BOREDK_Spotlight_Platform` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `BOREDK_Spotlight_Platform` ;

-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`product_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`product_status` (
  `status_id` CHAR(32) NOT NULL,
  `status_created_at` DATETIME NOT NULL,
  `status_updated_at` DATETIME NULL DEFAULT NULL,
  `status_name` VARCHAR(15) NOT NULL,
  PRIMARY KEY (`status_id`),
  UNIQUE INDEX `status_id_UNIQUE` (`status_id` ASC) VISIBLE,
  UNIQUE INDEX `status_name_UNIQUE` (`status_name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`products` (
  `product_id` CHAR(32) NOT NULL,
  `product_created_at` DATETIME NOT NULL,
  `product_updated_at` DATETIME NULL DEFAULT NULL,
  `product_title` VARCHAR(50) NOT NULL,
  `product_description` VARCHAR(500) NOT NULL,
  `product_image` CHAR(32) NULL DEFAULT NULL,
  `product_github_link` VARCHAR(1000) NULL DEFAULT NULL,
  `product_figma_link` VARCHAR(1000) NULL DEFAULT NULL,
  `product_host_link` VARCHAR(1000) NULL DEFAULT NULL,
  `product_confluence_link` VARCHAR(1000) NULL DEFAULT NULL,
  `product_squad_fk` CHAR(32) NOT NULL,
  `product_productowner_fk` CHAR(32) NOT NULL,
  `product_status_fk` CHAR(32) NOT NULL,
  PRIMARY KEY (`product_id`),
  UNIQUE INDEX `product_title_UNIQUE` (`product_title` ASC) VISIBLE,
  UNIQUE INDEX `product_id_UNIQUE` (`product_id` ASC) VISIBLE,
  INDEX `product_status_fk_idx` (`product_status_fk` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`squads`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`squads` (
  `squad_id` CHAR(32) NOT NULL,
  `squad_created_at` DATETIME NOT NULL,
  `squad_updated_at` DATETIME NULL DEFAULT NULL,
  `squad_name` VARCHAR(50) NOT NULL,
  `squad_description` VARCHAR(500) NOT NULL,
  `squad_tribe_fk` CHAR(32) NOT NULL,
  `squad_lead_user_fk` CHAR(32) NOT NULL,
  PRIMARY KEY (`squad_id`),
  UNIQUE INDEX `squad_id_UNIQUE` (`squad_id` ASC) VISIBLE,
  UNIQUE INDEX `squad_name_UNIQUE` (`squad_name` ASC) VISIBLE,
  INDEX `fk_squad_lead_id_idx` (`squad_id` ASC, `squad_lead_user_fk` ASC) VISIBLE,
  INDEX `fk_squad_leader_idx` (`squad_lead_user_fk` ASC) VISIBLE,
  INDEX `fk_squad_tribe_idx` (`squad_tribe_fk` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`tribes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`tribes` (
  `tribe_id` CHAR(32) NOT NULL,
  `tribe_created_at` DATETIME NOT NULL,
  `tribe_updated_at` DATETIME NULL DEFAULT NULL,
  `tribe_name` VARCHAR(50) NOT NULL,
  `tribe_description` VARCHAR(500) NOT NULL,
  `tribe_admin_fk` CHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`tribe_id`),
  UNIQUE INDEX `tribe_id_UNIQUE` (`tribe_id` ASC) VISIBLE,
  UNIQUE INDEX `tribe_name_UNIQUE` (`tribe_name` ASC) VISIBLE,
  INDEX `tribe_admin_fk_idx` (`tribe_admin_fk` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`user_roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`user_roles` (
  `role_id` CHAR(32) NOT NULL,
  `role_created_at` DATETIME NOT NULL,
  `role_updated_at` DATETIME NULL DEFAULT NULL,
  `role_name` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`role_id`),
  UNIQUE INDEX `id_role_UNIQUE` (`role_id` ASC) VISIBLE,
  UNIQUE INDEX `role_name_UNIQUE` (`role_name` ASC) VISIBLE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `BOREDK_Spotlight_Platform`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `BOREDK_Spotlight_Platform`.`users` (
  `user_id` CHAR(32) NOT NULL,
  `user_created_at` DATETIME NOT NULL,
  `user_updated_at` DATETIME NULL DEFAULT NULL,
  `user_username` VARCHAR(15) NOT NULL,
  `user_first_name` VARCHAR(50) NOT NULL,
  `user_last_name` VARCHAR(50) NOT NULL,
  `user_email` CHAR(18) NOT NULL,
  `user_password` VARCHAR(255) NOT NULL,
  `user_isSquadAdmin` TINYINT(1) NOT NULL,
  `user_isTribeAdmin` TINYINT(1) NOT NULL,
  `user_isSuperiorAdmin` TINYINT(1) NOT NULL,
  `user_avatar` BLOB NULL DEFAULT NULL,
  `user_squad_fk` CHAR(32) NOT NULL,
  `user_role_fk` CHAR(32) NOT NULL,
  `user_tribelead_fk` CHAR(32) NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) VISIBLE,
  UNIQUE INDEX `user_username_UNIQUE` (`user_username` ASC) VISIBLE,
  UNIQUE INDEX `user_email_UNIQUE` (`user_email` ASC) VISIBLE,
  INDEX `fk_user_squad_idx` (`user_squad_fk` ASC) VISIBLE,
  INDEX `fk_user_role_idx` (`user_role_fk` ASC) VISIBLE
)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

